import request from "./request"

const LIMIT_PER_PAGE = 8;

export const getPlaces = ({ pageParam, queryKey }) => {
  let url = `/api/places?page=${pageParam}&limit=${LIMIT_PER_PAGE}`
  const { search, category } = queryKey[1]

  if (search !== '' && search !== null) {
    url += `&search=${search}`
  }

  if (category !== '' && category !== null) {
    url += `&category=${category}`
  }

  return request({
    url: url,
    method: 'GET',
  })
}

export const getPlaceByID = (id) =>
  request({
    url: `/api/places/${id}`,
    method: 'GET',
  })
