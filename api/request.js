import axios from "axios";

const BASE_URL = process.env.EXPO_PUBLIC_API_URL;

const client = axios.create({
  baseURL: BASE_URL
})

export default async (options) => {
  const onSuccess = response => {
    console.log(response?.data);
    return response?.data;
  }

  const onError = error => {
    if (error.response) {
      console.log("ERROR_RESPONSE");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log("ERROR_REQUEST");
      console.log(error.request);
    } else {
      console.log("SOMETHING_WRONG");
      console.log('Error', error.message);
    }

    return Promise.reject(error.response?.data)
  }

  return client(options)
    .then(onSuccess)
    .catch(onError)
}