import { Entypo } from "@expo/vector-icons";
import Constants from 'expo-constants'
import { Image, SafeAreaView, ScrollView, StyleSheet, TextInput, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-paper";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Categories from "../components/Categories";
import Destinations from "../components/Destinations";
import { useInfiniteQuery } from "@tanstack/react-query";
import { getPlaces } from "../api/places";
import React, { useEffect, useState } from "react";

export default function HomeScreen() {
  const [category, setCategory] = useState('')
  const [search, setSearch] = useState('')
  const [debounceSearch, setDebounceSearch] = useState(null)

  // debounce search
  useEffect(() => {
    const timeoutID = setTimeout(() => {
      setDebounceSearch(search)
    }, 500);

    return () => clearTimeout(timeoutID)
  }, [search, 500])

  const { isLoading, isError, error, data, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage } = useInfiniteQuery({
    queryKey: ['places', {search: debounceSearch, category: category}],
    queryFn: getPlaces,
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      return lastPage.next_page_url ? lastPage.current_page + 1 : null
    },
  })

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <ScrollView showsHorizontalScrollIndicator={false} contentContainerStyle={styles.container}>
        {/* avatar */}
        <View style={[styles.row, styles.avatarContainer]}>
          <Text style={[styles.boldText, styles.headingText]}>Let's Discover</Text>
          <TouchableOpacity>
            <Image source={require('../assets/images/avatar.png')} style={styles.avatarImage} />
          </TouchableOpacity>
        </View>

        {/* search bar */}
        <View style={styles.searchContainer}>
          <View style={styles.searchBar}>
            <Entypo name="magnifying-glass" size={20} color="gray" />
            <TextInput
              placeholder="Search destination"
              placeholderTextColor="gray"
              style={styles.searchInput}
              value={search}
              onChangeText={setSearch}
            />
          </View>
        </View>

        {/* categories */}
        <View style={styles.categoriesContainer}>
          <Categories onSelect={(cat) => setCategory(cat)} />
        </View>

        {/* destinations */}
        {isLoading ? (
          <Text>Loading...</Text>
        ) : isError ? (
          <Text>Error Loading Data: {error?.message}</Text>
        ) : data?.pages.length === 0 ? (
          <Text>No Data</Text>
        ) : (
          <View>
            {data.pages.map((group, i) => (
              <React.Fragment key={i}>
                {/* {group.data.map((project) => (
                  <p key={project.id}>{project.name}</p>
                ))} */}
                <Destinations places={group?.data || []} />
              </React.Fragment>
            ))}
            {/* <Destinations places={data?.data || []} /> */}

            <View>
              <TouchableOpacity
                onPress={() => fetchNextPage()}
                disabled={!hasNextPage || isFetchingNextPage}
              >
                <Text style={{textAlign: 'center'}}>
                {isFetchingNextPage
                  ? 'Loading more...'
                  : hasNextPage
                    ? 'Load More'
                    : 'Nothing more to load'}
                    </Text>
              </TouchableOpacity>
            </View>
            <Text style={{textAlign: 'center'}}>{isFetching && !isFetchingNextPage ? 'Fetching...' : null}</Text>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  avatarContainer: {
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 40,
  },
  headingText: {
    fontSize: wp(7),
    color: '#374151', // text-neutral-700
  },
  boldText: {
    fontWeight: 'bold',
  },
  avatarImage: {
    height: wp(12),
    width: wp(12),
  },
  searchContainer: {
    marginHorizontal: 20,
    marginBottom: 16,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F3F4F6', // bg-neutral-100
    borderRadius: 9999, // full-rounded
    padding: 16,
    paddingLeft: 24,
  },
  searchInput: {
    flex: 1,
    fontSize: 16,
    marginBottom: 4,
    paddingLeft: 4,
    letterSpacing: 1.25,
  },
  categoriesContainer: {
    marginBottom: 16,
  },
})