import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { LinearGradient } from 'expo-linear-gradient'
import { useNavigation } from '@react-navigation/native';

const theme = {
  bg: (level) => `rgba(249, 115, 22, ${level})`,
};

export default function WelcomeScreen() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>

      {/* background image */}
      <Image
        source={require('../assets/images/welcome.png')}
        style={styles.backgroundImage}
      />

      {/* content & gradient */}
      <View style={styles.content}>
        <LinearGradient
          colors={['transparent', 'rgba(3,105,161,0.8)']}
          style={{ width: wp(100), height: hp(60), position: 'absolute', bottom: 0 }}
          start={{ x: 0.5, y: 0 }}
          end={{ x: 0.5, y: 1 }}
        />
        <View style={styles.textContainer}>
          <Text style={[styles.title, { fontSize: wp(10) }]}>Dolan Banten</Text>
          <Text style={[styles.subtitle, { fontSize: wp(4) }]}>
            Experience the world's best adventure around the world with us
          </Text>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate("Home")} style={[styles.button, { backgroundColor: theme.bg(1) }]}>
          <Text style={[styles.buttonText, { fontSize: wp(5.5) }]}>Let's go!</Text>
        </TouchableOpacity>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'flex-end',
  },
  backgroundImage: {
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  content: {
    padding: 20,
    paddingBottom: 40,
    justifyContent: 'space-between',
  },
  textContainer: {
    marginBottom: 20,
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
  },
  subtitle: {
    color: '#D1D5DB',
    fontWeight: '500',
  },
  button: {
    alignSelf: 'center',
    padding: 12,
    paddingHorizontal: 48,
    borderRadius: 9999, // full-rounded
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
})
