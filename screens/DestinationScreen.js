import { View, Text, Image, TouchableOpacity, ScrollView, Platform, StyleSheet } from 'react-native';
import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { AntDesign, Entypo, Feather, FontAwesome } from '@expo/vector-icons';
import { useQuery } from '@tanstack/react-query';
import { getPlaceByID } from '../api/places';

const ios = Platform.OS == 'ios';
const topMargin = ios ? {} : { marginTop: 40 };

export default function DestinationScreen(props) {
  console.log(props);
  const itemID = props.route.params.item_id

  const { isPending, isError, error, data: item } = useQuery({
    queryKey: ['places', itemID],
    queryFn: () => getPlaceByID(itemID),
  })

  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      {isPending ? (
        <Text>Loading...</Text>
      ) : isError ? (
        <Text>Error Loading Data: {error?.message}</Text>
      ) : (
        <>
          {/* destination image */}
          <Image source={{ uri: item.thumbnail }} style={styles.image} />
          <StatusBar style='light' />

          {/* back button */}
          <SafeAreaView style={[styles.safeArea, topMargin]}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={styles.backButton}
            >
              <Entypo name="chevron-left" size={24} color="black" />
            </TouchableOpacity>
          </SafeAreaView>

          {/* title & description & booking button */}
          <View style={styles.detailsContainer}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollContent}>
              <View style={styles.header}>
                <Text style={[styles.title, { fontSize: wp(7) }]}>{item?.name}</Text>
                {/* <Text style={[styles.price, { fontSize: wp(7) }]}>{`$ ${item?.price}`}</Text> */}
              </View>
              <Text style={[styles.description, { fontSize: wp(3.7) }]}>{item?.descriptions}</Text>

              <View style={styles.infoContainer}>
                <View style={styles.infoItem}>
                  <AntDesign name="star" size={24} color="skyblue" />
                  <View style={styles.info}>
                    <Text style={[styles.infoText, { fontSize: wp(4.5) }]}>{item.rating.toPrecision(2)}</Text>
                    <Text style={styles.infoSubText}>Rating</Text>
                  </View>
                </View>
                <View style={styles.infoItem}>
                  <FontAwesome name="map-marker" size={24} color="#f87171" />
                  <View style={styles.info}>
                    <Text style={[styles.infoText, { fontSize: wp(4.5) }]}>{item.distance}</Text>
                    <Text style={styles.infoSubText}>Distance</Text>
                  </View>
                </View>
                <View style={styles.infoItem}>
                  <Feather name="sun" size={24} color="orange" />
                  <View style={styles.info}>
                    <Text style={[styles.infoText, { fontSize: wp(4.5) }]}>{item.weather}</Text>
                    <Text style={styles.infoSubText}>Sunny</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
            <TouchableOpacity style={styles.bookButton}>
              <Text style={[styles.bookButtonText, { fontSize: wp(5.5) }]}>Book now</Text>
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    width: wp(100),
    height: hp(55),
  },
  safeArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    position: 'absolute',
  },
  backButton: {
    padding: 8,
    borderRadius: 9999,
    marginLeft: 16,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
  favouriteButton: {
    padding: 8,
    borderRadius: 9999,
    marginRight: 16,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
  detailsContainer: {
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    backgroundColor: 'white',
    paddingTop: 24,
    marginTop: -56,
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  scrollContent: {
    paddingBottom: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginBottom: 10,
  },
  title: {
    flex: 1,
    fontWeight: 'bold',
    color: '#374151',
  },
  price: {
    fontWeight: '600',
    color: '#f97316',
  },
  description: {
    color: '#374151',
    marginBottom: 20,
    letterSpacing: 0.5,
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 4,
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  info: {
    marginLeft: 8,
  },
  infoText: {
    fontWeight: 'bold',
    color: '#374151',
  },
  infoSubText: {
    color: '#6B7280',
  },
  bookButton: {
    height: wp(15),
    width: wp(50),
    marginBottom: 24,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 9999,
    backgroundColor: 'rgba(249, 115, 22, 0.8)'
  },
  bookButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});
