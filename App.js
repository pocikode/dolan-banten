import { Provider as PaperProvider } from 'react-native-paper'
import AppNavigation from './navigation'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const queryClient = new QueryClient()

export default function App() {
  return (
    <PaperProvider>
      <QueryClientProvider client={queryClient}>
        <AppNavigation />
      </QueryClientProvider>
    </PaperProvider>
  );
}