import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { LinearGradient } from 'expo-linear-gradient';
// import { HeartIcon } from 'react-native-heroicons/solid';
import { useNavigation } from '@react-navigation/native';

export default function Destinations({ places }) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      {places.map((item, index) => (
        <DestinationCard navigation={navigation} item={item} key={index} />
      ))}
    </View>
  );
}

const DestinationCard = ({ item, navigation }) => {
  const [isFavourite, toggleFavourite] = useState(false);
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Destination', { item_id: item._id })}
      style={[styles.card, { width: wp(44), height: wp(65) }]}
    >
      <Image
        source={{ uri: item.thumbnail }}
        // source="https://picsum.photos/seed/696/3000/2000"
        style={[styles.image, { width: wp(44), height: wp(65) }]}
      />
      <LinearGradient
        colors={['transparent', 'rgba(0,0,0,0.8)']}
        style={[styles.gradient, { width: wp(44), height: hp(15) }]}
        start={{ x: 0.5, y: 0 }}
        end={{ x: 0.5, y: 1 }}
      />
      {/* <TouchableOpacity
        onPress={() => toggleFavourite(!isFavourite)}
        style={styles.favouriteButton}
      >
        <HeartIcon size={wp(5)} color={isFavourite ? 'red' : 'white'} />
      </TouchableOpacity> */}
      <Text style={[styles.title, { fontSize: wp(4) }]}>{item.name}</Text>
      <Text style={[styles.description, { fontSize: wp(2.2) }]}>{item.short_description}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  card: {
    justifyContent: 'flex-end',
    position: 'relative',
    padding: 16,
    paddingVertical: 24,
    marginBottom: 20,
  },
  image: {
    position: 'absolute',
    borderRadius: 35,
  },
  gradient: {
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 35,
    borderBottomRightRadius: 35,
  },
  favouriteButton: {
    backgroundColor: 'rgba(255,255,255,0.4)',
    position: 'absolute',
    top: 8,
    right: 12,
    borderRadius: 9999,
    padding: 8,
  },
  title: {
    color: 'white',
    fontWeight: '600',
  },
  description: {
    color: 'white',
  },
});
