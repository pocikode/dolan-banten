import { View, Text, TouchableOpacity, ScrollView, Image, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { theme } from '../theme';
import { categoriesData } from '../constants';

export default function Categories({ onSelect }) {
  const [selected, setSelected] = useState('')

  const handleSelect = (param) => {
    if (selected === param) {
      setSelected('')
      onSelect('')
    } else {
      setSelected(param)
      onSelect(param)
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={[styles.headerText, { fontSize: wp(4) }]}>Categories</Text>
        {/* <TouchableOpacity>
          <Text style={[styles.seeAllText, { fontSize: wp(4) }]}>See all</Text>
        </TouchableOpacity> */}
      </View>
      <ScrollView
        horizontal
        contentContainerStyle={styles.scrollContainer}
        showsHorizontalScrollIndicator={false}
      >
        {categoriesData.map((cat, index) => (
          <TouchableOpacity key={index} style={[styles.categoryItem]} onPress={() => handleSelect(cat.param)}>
            <Image source={cat.image} style={[styles.categoryImage, { width: wp(20), height: wp(19), opacity: selected === cat.param ? 0.5 : 1.0 }]} />
            <Text
              style={[styles.categoryText, selected === cat.param ? styles.categoryTextActive : '']}
            >{cat.title}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 10,
  },
  headerText: {
    fontWeight: '600',
    color: '#374151', // text-neutral-700
  },
  seeAllText: {
    fontWeight: '600',
  },
  scrollContainer: {
    paddingHorizontal: 15,
  },
  categoryItem: {
    alignItems: 'center',
    marginRight: 16,
  },
  categoryImage: {
    borderRadius: 24,
  },
  categoryText: {
    marginTop: 8,
    fontWeight: '500',
    fontSize: wp(3),
    color: '#374151',
  },
  categoryTextActive: {
    color: 'rgb(14 165 233)',
    fontWeight: '700',
  }
});
