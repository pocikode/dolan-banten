export const categoriesData = [
    {
        title: 'Wisata Alam',
        param: 'wisata_alam',
        image: require('../assets/images/categories/ocean.png')
    },
    {
        title: 'Wisata Budaya',
        param: 'wisata_budaya',
        image: require('../assets/images/categories/mountain.png')
    },
    {
        title: 'Wisata Keluarga',
        param: 'wisata_keluarga',
        image: require('../assets/images/categories/camp.png')
    },
    {
        title: 'Wisata Kuliner',
        param: 'wisata_kuliner',
        image: require('../assets/images/categories/sunset.png')
    },
    {
        title: 'Wisata Pasangan',
        param: 'wisata_pasangan',
        image: require('../assets/images/categories/hiking.png')
    },
    // {
    //     title: 'Beach',
    //     image: require('../assets/images/categories/beach.png')
    // },
    // {
    //     title: 'Forest',
    //     image: require('../assets/images/categories/forest.png')
    // },
    
]
